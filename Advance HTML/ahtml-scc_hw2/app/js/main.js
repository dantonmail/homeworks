function burgerOpen() {
  const burger = document.querySelector(".top-menu__burger");
  let topMenu = document.querySelector(".top-menu__list");

  burger.addEventListener("click", () => {
    burger.classList.toggle("top-menu__burger-active");
    topMenu.classList.toggle("top-menu__list-active");
  });
}
burgerOpen();
