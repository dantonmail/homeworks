"use strict";

function testWebP(callback) {
  var webP = new Image();
  webP.onload = webP.onerror = function () {
    callback(webP.height == 2);
  };
  webP.src =
    "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

testWebP(function (support) {
  if (support == true) {
    document.querySelector("body").classList.add("webp");
  } else {
    document.querySelector("body").classList.add("no-webp");
  }
});

// @@include("_alert.js");

function burgerOpen() {
  const burger = document.querySelector(".top-menu__burger");

  let topMenu = document.querySelector(".top-menu__list");

  burger.addEventListener("click", () => {
    burger.classList.toggle("top-menu__burger-active");
    topMenu.classList.toggle("top-menu__list-active");
  });
}
burgerOpen();
