import React from "react";
import Card from "../Card";

function Cart({ data = [] }) {
  if (data === null || data.length === 0) {
    return <h1>There are no item to displey, please make your choice</h1>;
  } else if (data.length !== 0) {
    return data.map((data) => {
      return <Card data={data} delButCart="true" key={`${data.id}`} />;
    });
  }
}

export default Cart;
