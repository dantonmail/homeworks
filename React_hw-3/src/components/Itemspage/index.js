import React from "react";
import Itemlist from "../Itemlist";
import { useState, useEffect } from "react";

function Itemspage({
  setUserFavorite = () => {},
  setUserCart = () => {},
  favorite = [],
  cart = [],
}) {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setData(data);
      });
  }, []);

  return (
    <Itemlist
      setUserFavorite={setUserFavorite}
      setUserCart={setUserCart}
      data={data}
      favorite={favorite}
      cart={cart}
    />
  );
}

export default Itemspage;
