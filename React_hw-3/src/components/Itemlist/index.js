import Card from "../Card";
import React from "react";

function Itemlist({
  data = [],
  addItem = () => {},
  setUserFavorite = () => {},
  setUserCart = () => {},
}) {
  return (
    <>
      {data.map((data) => {
        return (
          <Card
            data={data}
            key={`${data.id}`}
            setUserFavorite={setUserFavorite}
            setUserCart={setUserCart}
            addButCart="true"
            addEmptyrtStar="true"
          />
        );
      })}
    </>
  );
}

export default Itemlist;
