import React from "react";
import Itemspage from "../Itemspage";
import Nav from "../Nav";
import { useState } from "react";
import Cart from "../Cart";
import Favorite from "../Favorite";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

function App() {
  //объявление стэйт для массива favorite и запрос на получение данных из Local Storafe
  const [favorite, setFavorute] = useState(
    JSON.parse(localStorage.getItem("favorite"))
  );

  // функция-колбек для получения данных из компоненты Card и изменения их в стэйте для массива Favorite
  function setUserFavorite(favorite) {
    setFavorute(favorite);
  }

  //объявление стэйт для массива Cart и запрос на получение данных из Local Storafe
  const [cart, setCart] = useState(JSON.parse(localStorage.getItem("cart")));

  // функция-колбек для получения данных из компоненты Card и изменения их в стэйте для массива Cart
  function setUserCart(cart) {
    setCart(cart);
  }

  return (
    <Router>
      <div className="App">
        <div className="main__container">
          <div className="header">
            <Nav />
          </div>
          <div className="cards">
            <Switch>
              <Route exact path="/">
                <Itemspage
                  setUserFavorite={setUserFavorite}
                  setUserCart={setUserCart}
                />
              </Route>

              <Route exact path="/favorite">
                <Favorite data={favorite} />
              </Route>
              <Route exact path="/cart">
                <Cart data={cart} />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
