import React from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { removeDataCart } from "../../redux/actions";
import { addUserdata } from "../../redux/actions";

function Form() {
  const dispatch = useDispatch();
  const cart = useSelector((store) => {
    //получение данных массива cart из store
    return store.cart;
  });

  const cartClear = () => {
    //очищение данных массива cart из store
    const newCart = [];
    dispatch(removeDataCart(newCart));
  };

  const showItem = (cart) => {
    //показ данных о покупке в консоли
    const result = [];
    cart.forEach((e) => {
      result.push(e.name);
    });
    console.log("ваша покупка:", result.join("; "));
  };

  const validationSchema = yup.object().shape({
    name: yup.string().typeError("должно быть строкой").required("Обязательно"),
    secondName: yup
      .string()
      .typeError("должно быть строкой")
      .required("Обязательно"),
    age: yup.number().typeError("должно быть числом").required("Обязательно"),
    adress: yup
      .string()
      .typeError("должно быть строкой")
      .required("Обязательно"),
    phone: yup.number().typeError("должно быть числом").required("Обязательно"),
  });

  return (
    <div>
      <Formik
        initialValues={{
          name: "",
          secondName: "",
          age: "",
          adress: "",
          phone: "",
        }}
        validateOnBlur
        onSubmit={(values) => {
          console.log("name:", values.name);
          console.log("secondName:", values.secondName);
          console.log("age:", values.age);
          console.log("adress:", values.adress);
          console.log("phone:", values.phone);
          dispatch(addUserdata(values)); //добавление данных о пользователе в стору
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          isValid,
          handleSubmit,
          dirty,
        }) => (
          <div className={"form__content"}>
            <p>
              <label htmlFor={"name"}>Имя</label>
              <br />
              <input
                className={"input"}
                type={"text"}
                name={"name"}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
            </p>
            {touched.name && errors.name && (
              <p className={"error"}> {errors.name}</p>
            )}
            {/* ======= */}

            <p>
              <label htmlFor={"secondName"}>Фамилия</label>
              <br />
              <input
                className={"input"}
                type={"text"}
                name={"secondName"}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.secondName}
              />
            </p>
            {touched.secondName && errors.secondName && (
              <p className={"error"}> {errors.secondName}</p>
            )}
            {/* ======== */}

            {/* =========== */}
            <p>
              <label htmlFor={"age"}>Возраст</label>
              <br />
              <input
                className={"input"}
                type={"text"}
                name={"age"}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.age}
              />
            </p>
            {touched.age && errors.age && (
              <p className={"error"}> {errors.age}</p>
            )}
            {/* =========== */}

            {/* =========== */}
            <p>
              <label htmlFor={"adress"}>Адрес</label>
              <br />
              <input
                className={"input"}
                type={"text"}
                name={"adress"}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.adress}
              />
            </p>
            {touched.adress && errors.adress && (
              <p className={"error"}> {errors.adress}</p>
            )}
            {/* =========== */}

            {/* =========== */}
            <p>
              <label htmlFor={"phone"}>Телефон</label>
              <br />
              <input
                className={"input"}
                type={"text"}
                name={"phone"}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
              />
            </p>
            {touched.phone && errors.phone && (
              <p className={"error"}> {errors.phone}</p>
            )}
            {/* =========== */}

            <button
              disabled={!isValid && !dirty}
              // onClick={handleSubmit}
              type={"submit"}
              onClick={() => {
                handleSubmit();
                cartClear();
                showItem(cart);
              }}
            >
              Отправить
            </button>
          </div>
        )}
      </Formik>
    </div>
  );
}

export default Form;
