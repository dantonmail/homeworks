import React from "react";
import Card from "../Card";

function Cart({ data = [] }) {
  // if (data.length === 0) {
  //   return <h1>There are no item to displey, please make your choice</h1>;
  // } else if (data) {
  //   return data.map((data) => {
  //     return <Card data={data} delButCart={true} key={`${data.id}`} />;
  //   });
  // }

  const full = (data) => {
    return data.map((data) => {
      return <Card data={data} delButCart={true} key={`${data.id}`} />;
    });
  };
  const empty = () => {
    return <h1>There are no item to displey, please make your choice</h1>;
  };

  return (data.length === 0 && empty()) || (data && full(data));
}

export default Cart;
