"use strict";

let tab = function () {
  let tabNav = document.querySelectorAll(".tabs-title");
  let tabContent = document.querySelectorAll(".tabs-panel");
  let tabName;

  tabNav.forEach((item) => {
    item.addEventListener("click", selectTabNav);
  });

  function selectTabNav() {
    tabNav.forEach((item) => {
      item.classList.remove("active");
    });
    this.classList.add("active");
    tabName = this.getAttribute("data-number");
    selectTabContent(tabName);
    console.log(tabName);
  }

  function selectTabContent(tabName) {
    tabContent.forEach((item) => {
      item.classList.contains(tabName)
        ? item.classList.add("active")
        : item.classList.remove("active");
    });
  }
};

tab();

// ==============

// document.querySelector('.tab-content[data-tab-title="' + event.target.dataset.tabTitle + '"]').classList.add('active');
