function ChangeTheme() {
  if (!localStorage.theme) localStorage.theme = "light";
  document.body.className = localStorage.theme;

  let changThemeBtb = document.getElementById("changthemebtn");

  changThemeBtb.onclick = () => {
    document.body.classList.toggle("dark");
    localStorage.theme = document.body.className || "light";
  };
}

ChangeTheme();
