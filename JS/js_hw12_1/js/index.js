"use strict";

// function slider() {
//   let slides = document.querySelectorAll(".image-to-show");
//   let currentSlide = 0;
//   let slideInterval = setInterval(nextSlide, 3000);

//   function nextSlide() {
//     slides[currentSlide].classList.toggle("active");
//     currentSlide = (currentSlide + 1) % slides.length;
//     slides[currentSlide].classList.toggle("active");
//   }

//   let playing = true;
//   let playButton = document.querySelector(".play");
//   let pauseButton = document.querySelector(".pause");
//   let btnControls = document.querySelector(".controls");

//   function pauseSlideshow() {
//     playing = false;
//     clearInterval(slideInterval);
//   }

//   btnControls.addEventListener("click", (e) => {
//     if (e.target == pauseButton && playing) {
//       pauseSlideshow();
//       pauseButton.disabled = true;
//       pauseButton.classList.add("not-active");
//       playButton.disabled = false;
//       playButton.classList.remove("not-active");
//     }

//     if (e.target == playButton && !playing) {
//       playSlideshow();
//       playButton.disabled = true;
//       playButton.classList.add("not-active");
//       pauseButton.disabled = false;
//       pauseButton.classList.remove("not-active");
//     }
//   });

//   function playSlideshow() {
//     playing = true;
//     slideInterval = setInterval(nextSlide, 3000);
//   }
// }

// slider();

// 1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
// -первая функция выполняется разово, вторая функция выполнется циклично через промежуток времени указанный в параметрах функции.
// 2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// -функция сработает не мгновенно, поскольку она попадет в "стек" ожидания и сработает тогда, когда на это будет время. сначала выполняются "синхронные операции", потом выполняется "асинхронные операции".
// 3.Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
// -если этого не сделать, функция останется 'жить' а значит будет работать и потреблять ресурсы системы.

// function slider() {
//   let slides = document.querySelectorAll(".image-to-show");
//   let currentSlide = 0;
//   let slideInterval = setInterval(nextSlide, 3000);

//   function nextSlide() {
//     slides[currentSlide].classList.toggle("active");
//     currentSlide = (currentSlide + 1) % slides.length;
//     slides[currentSlide].classList.toggle("active");
//   }

//   let playing = true;
//   let playButton = document.querySelector(".play");
//   let pauseButton = document.querySelector(".pause");
//   let btnControls = document.querySelector(".controls");

//   btnControls.addEventListener("click", (e) => {
//     if (e.target == pauseButton && playing) {
//       clearInterval(slideInterval);
//       pauseButton.disabled = true;
//       pauseButton.classList.add("not-active");
//       playButton.disabled = false;
//       playButton.classList.remove("not-active");
//     } else {
//       slideInterval = setInterval(nextSlide, 3000);
//       playButton.disabled = true;
//       playButton.classList.add("not-active");
//       pauseButton.disabled = false;
//       pauseButton.classList.remove("not-active");
//     }
//   });
// }

// slider();

// =============

function slider() {
  let slides = document.querySelectorAll(".image-to-show");
  let currentSlide = 0;
  let slideInterval = setInterval(nextSlide, 3000);

  function nextSlide() {
    slides[currentSlide].classList.toggle("active");
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].classList.toggle("active");
  }

  let btnControls = document.querySelector(".controls");
  let btnName;

  btnControls.addEventListener("click", (e) => {
    if (btnControls) {
      e.target.disabled = true;
      e.target.classList.add("not-active");
      btnName = e.target.dataset.number;
      selectTabContent(btnName);
    }
  });
  function selectTabContent(btnName) {
    if (btnName == "0") {
      slideInterval = setInterval(nextSlide, 3000);
      btnControls.children[1].disabled = false;
      btnControls.children[1].classList.remove("not-active");
    } else {
      clearInterval(slideInterval);
      btnControls.children[0].disabled = false;
      btnControls.children[0].classList.remove("not-active");
    }
  }
}

slider();
