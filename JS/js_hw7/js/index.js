"use strict";

let arr = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

let div = document.querySelector(".cityName");
function createList(arr) {
  let newArr = arr
    .map((item) => {
      if (Array.isArray(item)) {
        return `${createList(item)}`;
      } else {
        return `<li>${item}</li>`;
      }
    })
    .join("");
  return `<ul>${newArr}</ul>`;
}
div.insertAdjacentHTML("beforeend", createList(arr));

let timer = document.querySelector(".timer");
setInterval(() => {
  if (+timer.innerHTML > 0) {
    timer.innerHTML -= 1;
  } else {
    clearInterval(timer);
    document.body.innerHTML = "";
  }
}, 1000);

// ++++++++++++++++++++

// DOM это отображние HTML страницы  в формате объектной модели документа.(в виде дерева тегов)
