"user strict";

function createNewUser() {
  let name = prompt("назовите ваше имя", "");
  let surname = prompt("назовите вашу фамилию", "");
  let user = {
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },

    setFirstName() {
      Object.defineProperty(newUser, "firstName", {
        value: prompt("назовите имя для замены", ""),
      });
    },
    setLastName() {
      Object.defineProperty(newUser, "lastName", {
        value: prompt("назовите фамилию для замены", ""),
      });
    },
  };
  Object.defineProperty(user, "firstName", {
    writable: false,
    configurable: true,
    value: name,
  }),
    Object.defineProperty(user, "lastName", {
      writable: false,
      configurable: true,
      value: surname,
    });
  return user;
}
let newUser = createNewUser();

console.log(newUser.getLogin());

newUser.setFirstName();
newUser.setLastName();

console.log(newUser.getLogin());

// ============

// function createNewUser() {
//   let name = prompt("назовите ваше имя", "");
//   let surname = prompt("назовите вашу фамилию", "");
//   let user = {
//     getLogin() {
//       return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
//     },
//     setFirstName(firstName) {
//       Object.defineProperty(this, "firstName", {
//         value: firstName,
//       });
//     },
//     setLastName(lastName) {
//       Object.defineProperty(this, "lastName", {
//         value: lastName,
//       });
//     },
//   };
//   Object.defineProperty(user, "firstName", {
//     writable: false,
//     configurable: true,
//     value: name,
//   }),
//     Object.defineProperty(user, "lastName", {
//       writable: false,
//       configurable: true,
//       value: surname,
//     });
//   return user;
// }
// let newUser = createNewUser();
// newUser.setFirstName("New Name");
// newUser.setLastName("New Lastname");
// console.log(newUser);
// console.log(newUser.getLogin());
