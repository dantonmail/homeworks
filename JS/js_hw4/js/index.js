"use strict";

function createNewUser() {
  let name = prompt("назовите ваше имя", "");
  let surname = prompt("назовите вашу фамилию", "");
  return {
    firstName: name,
    lastName: surname,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };
}

let newUser = createNewUser();

console.log(newUser.getLogin());

// ++++++++++++++++++++

// метод объекта это функция внутри объекта, которая явлется свойством этого объекта. Как правило, методу объекта необходим доступ к информации, которая хранится в объекте. ДЛя доступа к информации внутри объекта используют ключевое слово this. Значение this – это объект «перед точкой», который использовался для вызова метода.
