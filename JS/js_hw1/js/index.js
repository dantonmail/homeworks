"use strict";

const AGE_MIN = 18;
const AGE_MAX = 22;

let userName = prompt("как твое имя", " ");

while (!userName.match(/^[a-zA-Z]+$/)) {
  userName = prompt("Как тебя зовут?", "");
  if (!userName) break;
}

let userAge = +prompt("сколько тебе лет?", " ");
while (isNaN(userAge)) {
  userAge = prompt("Сколько тебе лет?", "");
  if (!userAge) break;
}

let messageYes = "Welcome, " + userName;
let messageNo = "You are not allowed to visit this website";

if (userAge < AGE_MIN) {
  alert(messageNo);
} else if (userAge >= AGE_MIN && userAge <= AGE_MAX) {
  let sure = confirm("Are you sure you want to continue?");
  if (sure) {
    alert(messageYes);
  } else {
    alert(messageNo);
  }
} else {
  alert(messageYes);
}

// Объявление переменной при помощи "VAR", считается устаревшим методом и поэтому при объявлении переменных с использованием синтаксиса ES6 (ES2015) такой метод считается плохим тоном. Кроме того, такое объявления переменной может нести некоторые неудобства связанные с тем, что переменная будет VAR видна не только в рамках блока в котором она объявлена, но и за его пределами. (т.е. станет глобальной и видимой там, где она не нужна)

// Пеменная "Let" может быть задана один раз и ее значение может в последствии меняться на другое по необходимости.
// Переменная "Сonst" задаёт константу, то есть переменную, которую нельзя менять.
