"use strict";

let tabControl = function () {
  let tab = document.querySelector(".tabs");
  let tabContent = document.querySelector(".tabs-content");
  let tabName;
  if (tab) {
    tab.addEventListener("click", (e) => {
      if (e.target.classList.contains("tabs-title")) {
        document.querySelector(".tabs-title.active").classList.remove("active");
        e.target.classList.add("active");
        tabName = e.target.getAttribute("data-number");
        selectTabContent(tabName);
      }

      function selectTabContent(tabName) {
        document.querySelector(".tabs-panel.active").classList.remove("active");
        tabContent.children[tabName].classList.add("active");
      }
    });
  }
};

tabControl();
