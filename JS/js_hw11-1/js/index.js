"use strict";

function checkLetter() {
  let btn = document.querySelectorAll(".btn");
  document.addEventListener("keydown", function (e) {
    return btn.forEach((item) => {
      let btnName = item.getAttribute("buttonName");
      if (btnName === e.code) {
        item.classList.add("active");
      } else {
        item.classList.remove("active");
      }
    });
  });
}

checkLetter();

// =============
// Поскольку при работе с инпутом, пользователь в том числе может воспользоваться вставкой необходимых значения например использую мышку, при помощи правого клика и меню, без единого нажатия клавиши,то мы не сможем адекватно отследить его действия.
// Инпут имеет свои стандартые события и лучше пользоваться имеено ими.
