"use strict";

function createNewUser() {
  let name = prompt("назовите ваше имя", "");
  let surname = prompt("назовите вашу фамилию", "");
  let age = prompt('назовите вашу дату рождения в формате "dd.mm.yyyy"', "");

  return {
    firstName: name,
    lastName: surname,
    birthday: age,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      return (
        ((new Date().getTime() -
          new Date(
            this.birthday.slice(-4) +
              this.birthday.slice(2, 3) +
              this.birthday.slice(3, 6) +
              this.birthday.slice(0, 2)
          )) /
          (24 * 3600 * 365.25 * 1000)) |
        0
      );
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
}

let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());

// ++++++++++++++++++++

// экранирование в js выполняется при помощи слеш и применяется в строке кода, когда нам нужно вставить в строку спец символ (например кавычки, tab, слеш). Тогда интерпритатор не воспринимает экранированный символ как часть скрипта.
