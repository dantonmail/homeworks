"use strict";

let arr = ["hello", "world", 23, "23", 73, null];
let b = "string";

console.log(arr);

function filterBy(arr, b) {
  let result = [];

  for (let str of arr) {
    if (typeof str !== b) {
      result.push(str);
    }
  }
  return result;
}
let newArr = filterBy(arr, b);
console.log(newArr);

// ++++++++++++++++++++

// Цикл foreach похож на стандартный цикл for, но он позволяет укоротить код для перебора массивов. Цикл foreach — это функция, запускающая другую функцию (обратный вызов) для каждого элемента массива. Эта функция получает три параметра ( значение/элемент массива, индекс/номер элемента, массив который перебирается). Поэтому в цикле foreach не нужно объявлять локальные переменные для хранения индекса и значения элемента. Так же в этом цикле нельзя использовать операторы Break (для досрочного выхода из цикла ) и continue, поскольку сам цикл ничего не возвращает.

// let filterBy = (arr, type) => arr.filter((item) => typeof item !== type);
// console.log(filterBy(["hello", "world", 23, "23", 73, null], "string"));
