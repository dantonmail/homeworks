"use strict";

let findCategory = function () {
  let tab = document.querySelector(".nav-selection");
  let tabContent = document.querySelectorAll(".anchor");
  let tabName;
  let name;

  if (tab) {
    tab.addEventListener("click", (e) => {
      if (e.target.classList.contains("nav-link")) {
        tabName = e.target.getAttribute("data-mame");
        selectTabContent(tabName);
      }

      function selectTabContent(tabName) {
        tabContent.forEach((item) => {
          if (
            item.classList.contains("anchor") &&
            item.getAttribute("data-mame") === tabName
          ) {
            name = item.getAttribute("data-mame");
            $("html,body")
              .stop()
              .animate({ scrollTop: $(`#${name}`).offset().top }, 3000);
            e.preventDefault();
          }
        });
      }
    });
  }
};

findCategory();

let btn = $("#button");

$(window).scroll(function () {
  if ($(window).scrollTop() > 300) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

btn.on("click", function (e) {
  e.preventDefault();
  $("html, body").animate({ scrollTop: 0 }, "300");
});

$(".button-hide ").click(function () {
  $(".top-rated").toggleClass("active");
});

// ===============

// $(function () {
//   $(".nav-link-news").on("click", function (e) {
//     $("html,body")
//       .stop()
//       .animate({ scrollTop: $("#posts").offset().top }, 3000);
//     e.preventDefault();
//   });
// });

// $(function () {
//   $(".nav-link-post").on("click", function (e) {
//     $("html,body")
//       .stop()
//       .animate({ scrollTop: $("#posts").offset().top }, 3000);
//     e.preventDefault();
//   });
// });

// $(function () {
//   $(".nav-link-clients").on("click", function (e) {
//     $("html,body")
//       .stop()
//       .animate({ scrollTop: $("#clients").offset().top }, 3000);
//     e.preventDefault();
//   });
// });

// $(function () {
//   $(".nav-link-rated").on("click", function (e) {
//     $("html,body")
//       .stop()
//       .animate({ scrollTop: $("#Top-rated").offset().top }, 3000);
//     e.preventDefault();
//   });
// });

// $(function () {
//   $(".nav-link-hot-news").on("click", function (e) {
//     $("html,body")
//       .stop()
//       .animate({ scrollTop: $("#hot-news").offset().top }, 3000);
//     e.preventDefault();
//   });
// });

// ============
