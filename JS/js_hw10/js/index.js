"use strict";

function makeRegistration() {
  let iconPassword = document.querySelectorAll(".icon-password");
  let btn = document.querySelector(".btn");
  let password = document.getElementById("Password");
  let confirmPassword = document.getElementById("ConfirmPassword");

  for (let i = 0; i < iconPassword.length; i++) {
    iconPassword[i].addEventListener("click", function (e) {
      let showPwd = this.parentNode.querySelector(".show-pwd");

      if (showPwd.getAttribute("type") == "password") {
        showPwd.setAttribute("type", "text");
        this.classList.toggle("fa-eye-slash");
      } else {
        showPwd.setAttribute("type", "password");
        this.classList.toggle("fa-eye-slash");
      }
    });
  }

  btn.addEventListener("click", function () {
    if (password.value != confirmPassword.value) {
      btn.insertAdjacentHTML(
        "afterend",
        `<div class="text">Нужно ввести одинаковые значения!!!</div>`
      );
    } else {
      alert("You are welcome");
    }
  });
}
makeRegistration();
