"use strict";

let input = document.querySelector(".input");
let del = document.querySelectorAll(".box_del");
let holderUp = document.querySelector(".holder_Up");
let holderDown = document.querySelector(".holder_Down");

function updateDelBtn() {
  del = document.querySelectorAll(".box_del");

  del.forEach((item) => {
    item.onclick = (e) => {
      e.target.parentNode.remove();
      input.value = "";
    };
  });
}

input.addEventListener("change", () => {
  let inputData = input.value;

  if (+inputData <= 0) {
    input.classList.add("arror");
    holderDown.innerHTML = "";
    holderDown.insertAdjacentHTML(
      "beforeend",
      `<div class="box_item_inner">
            <span class="box_text">Please enter correct price</span>
            <button class="box_del">x</button>
          </div>`
    );
  } else {
    input.classList.add("no_focus");
    holderUp.insertAdjacentHTML(
      "afterbegin",
      `<div class="box_item_inner">
            <span class="box_text">Текущая цена:$  ${inputData}</span>
            <button class="box_del">x</button>
          </div>`
    );
  }

  updateDelBtn();
});
updateDelBtn();

// +++++++++++++++
// обработчик событий это функция, которая срабатывает при наступлении определенного события в браузере и производит запрограммированные действия.
