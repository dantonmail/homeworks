"use strict";

const sliderActions = {
  play: function (cb) {
    return setInterval(cb, 3000);
  },
  pause: function (intervalId) {
    clearInterval(intervalId);
  },
};

function slider() {
  let slides = document.querySelectorAll(".image-to-show");
  let currentSlide = 0;
  let slideInterval = sliderActions.play();

  function nextSlide() {
    slides[currentSlide].classList.toggle("active");
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].classList.toggle("active");
  }

  let btnControls = document.querySelector(".controls");

  btnControls.addEventListener("click", (e) => {
    const action = e.target.dataset.action;
    slideInterval = sliderActions[action](
      action === "pause" ? slideInterval : nextSlide
    );
    const actionElement = document.querySelector(".not-active");
    if (actionElement) {
      actionElement.disabled = false;
      actionElement.classList.remove("not-active");
    }
    e.target.disabled = true;
    e.target.classList.add("not-active");
  });
}

slider();

// 1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
// -первая функция выполняется разово, вторая функция выполнется циклично через промежуток времени указанный в параметрах функции.
// 2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// -функция сработает не мгновенно, поскольку она попадет в "стек" ожидания и сработает тогда, когда на это будет время. сначала выполняются "синхронные операции", потом выполняется "асинхронные операции".
// 3.Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
// -если этого не сделать, функция останется 'жить' а значит будет работать и потреблять ресурсы системы.
