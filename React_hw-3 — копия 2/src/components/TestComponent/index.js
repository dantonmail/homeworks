import Button from "../Button";
import { useState } from "react";

function TestComponent() {
  const [color, setColor] = useState(true);

  const changeColor = () => {
    setColor(!color);
  };

  return (
    <div className="card__addCart">
      <Button
        text={
          <svg
            className={
              (color && "card__svg_green") || (!color && "card__svg_red")
            }
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            viewBox="0 0 32 32"
          >
            <title>star</title>
            <path
              d="M16 23l-9 6 4-10-9-6h10l4-10 4 10h10l-9 6 4 10z"
              fill="currentColor"
              stroke="black"
            ></path>
          </svg>
        }
        bgc=""
        // onClick={() => {
        //   starHandlerAdd(data.id, data);
        // }}
        btnClName="card__star"
      ></Button>

      <button
        // onClick={() => {
        //   changeColor();
        // }}
        onClick={changeColor}
      >
        Сменить цвет
      </button>
    </div>
  );
}

export default TestComponent;
