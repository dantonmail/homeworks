import React from "react";
import Itemspage from "../Itemspage";
import Nav from "../Nav";
import { useState } from "react";
import Cart from "../Cart";
import Favorite from "../Favorite";
import TestComponent from "../TestComponent";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  // Redirect,
} from "react-router-dom";

function App() {
  const [favorite, setFavorute] = useState([]);
  //функция-колбек для прокидывания и возврата пропсов Favorite из Card
  function setUserFavorite(favorite) {
    setFavorute(favorite);
  }

  let [cart, setCart] = useState([]);
  //функция-колбек для прокидывания и возврата пропсов Cart из Card
  function setUserCart(cart) {
    setCart(cart);
  }

  return (
    <Router>
      <div className="App">
        <div className="main__container">
          <div className="header">
            <Nav />
          </div>
          <div className="cards">
            <Switch>
              <Route exact path="/">
                <Itemspage
                  setUserFavorite={setUserFavorite}
                  setUserCart={setUserCart}
                  favorite={favorite}
                  cart={cart}
                />
              </Route>

              <Route exact path="/favorite">
                <Favorite
                  addFullStar={true}
                  addEmptyrtStar={false}
                  data={favorite}
                />
              </Route>
              <Route exact path="/cart">
                <Cart data={cart} />
              </Route>

              <Route exact path="/testcomponent">
                <TestComponent data={cart} />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
