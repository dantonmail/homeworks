import Card from "../Card";
import React from "react";

function Itemlist({
  data = [],
  addItem = () => {},
  setUserFavorite = () => {},
  setUserCart = () => {},
  favorite = [],
  cart = [],
}) {
  return (
    <>
      {data.map((data) => {
        return (
          <Card
            data={data}
            key={`${data.id}`}
            // addItem={addItem}
            setUserFavorite={setUserFavorite}
            setUserCart={setUserCart}
            addButCart="true"
            addEmptyrtStar="true"
            dataFavorite={favorite}
            dataCart={cart}
          />
        );
      })}
    </>
  );
}

export default Itemlist;
