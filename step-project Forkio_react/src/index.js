import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import "./reset.scss"
import App from "./components/App/App.js";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

function burgerOpen() {
  const burger = document.querySelector(".header-topmenu__burger");
  let topMenu = document.querySelector(".header-topmenu__list");
  let bl = document.querySelector(".header-topmenu__burger__line");

  burger.addEventListener("click", () => {
    bl.classList.toggle("header-topmenu__burger__line-hide");
    burger.classList.toggle("header-topmenu__burger-active");
    topMenu.classList.toggle("header-topmenu__list-active");
    document.body.classList.toggle("lock");
  });

  document.addEventListener("click", outsideEvtListener);

  function outsideEvtListener(e) {
    if (
      e.target === topMenu ||
      topMenu.contains(e.target) ||
      e.target === burger ||
      e.target === bl
    ) {
      return;
    }
    bl.classList.remove("header-topmenu__burger__line-hide");
    burger.classList.remove("header-topmenu__burger-active");
    topMenu.classList.remove("header-topmenu__list-active");
    document.body.classList.remove("lock");
  }
}
burgerOpen();
