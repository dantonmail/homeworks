import React from "react";
import Aboutforkcontent from "../Aboutforkcontent";
import logo1 from "../../img/logo1.png";
import logo2 from "../../img/logo2.png";
import logo3 from "../../img/logo3.png";
import logo4 from "../../img/logo4.png";
import logo5 from "../../img/logo5.png";
import logo6 from "../../img/logo6.png";

function Aboutfork() {
  return (
    <section className="about-fork">
      <div className="container container_about_fork">
        <h2 className="section-title about-fork__title text_center">
          People Are Talking About Fork
        </h2>
        <div className="about-fork__content__wrapper">
          <Aboutforkcontent
            // image="../../img/logo1.png"
            image={logo1}
            TextAboutfork="Sed vestibulum scelerisque urna, eu finibus leo facilisis sit
            amet. Proin id dignissim magna. Sed varius urna et pulvinar
            venenatis."
          ></Aboutforkcontent>

          <Aboutforkcontent
            image={logo2}
            TextAboutfork="Donec euismod dolor ut ultricies consequat. Vivamus urna ipsum,
            rhoncus molestie neque ac, mollis eleifend nibh."
          ></Aboutforkcontent>

          <Aboutforkcontent
            image={logo3}
            TextAboutfork="In efficitur in velit et tempus. Duis nec odio dapibus, suscipit
            erat fringilla, imperdiet nibh. Morbi tempus auctor felis ac
            vehicula."
          ></Aboutforkcontent>

          <Aboutforkcontent
            image={logo4}
            TextAboutfork="Sed vestibulum scelerisque urna, eu finibus leo facilisis sit
            amet. Proin id dignissim magna. Sed varius urna et pulvinar
            venenatis."
          ></Aboutforkcontent>

          <Aboutforkcontent
            image={logo5}
            TextAboutfork="Praesent ut eros tristique, malesuada lectus vel, lobortis massa.
            Nulla faucibus lorem id arcu consequat faucibus."
          ></Aboutforkcontent>

          <Aboutforkcontent
            image={logo6}
            TextAboutfork="Fusce pharetra erat id odio blandit, nec  pharetra eros venenatis.
            Pellentesque porttitor cursus massa et vestibulum."
          ></Aboutforkcontent>
        </div>

        <span className="about-fork__bottom__text text_center">
          Duis lobortis arcu sed arcu tincidunt feugiat. Nulla nisi mauris,
          facilisis vitae aliquet id, imperdiet quis nibh. Donec eget elit eu
          libero tincidunt consequat nec elementum orci. Cum sociis natoque
          penatibus et magnis dis parturient montes, nascetur ridiculus mus.
        </span>
      </div>
    </section>
  );
}

export default Aboutfork;
