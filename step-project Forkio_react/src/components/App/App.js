import "./App.css";
import Header from "../Header";
import Aboutfork from "../Aboutfork";
import Features from "../Features";
import Presenttext from "../Presenttext";
import Pricing from "../Pricing";

function App() {
  return (
    <div>
      <Header />
      <Presenttext />
      <Features />
      <Aboutfork />
      <Pricing />
    </div>
  );
}

export default App;
