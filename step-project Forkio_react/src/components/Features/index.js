import icon1 from "../../img/icons/features-icon1.png";
import icon2 from "../../img/icons/features-icon2.png";
import icon3 from "../../img/icons/features-icon3.png";
import icon4 from "../../img/icons/features-icon4.png";
import icon5 from "../../img/features-editor.png";

function Features() {
  return (
    <section className="features">
      <div className="container container_features">
        <h2 className="section-title features__title text_center">
          &lt;Here is what you get&gt;
        </h2>
        <div className="features__content__wrapper">
          <div className="features__content__wrapper-box">
            <div className="features__content__box">
              <img className="features__content__icon" src={icon1} alt="" />
              <div className="features__content__text-box">
                <span className="features__content__title text_center">
                  Creatred to Make The Web Better
                </span>
                <span className="features__content__text text_center">
                  Aenean cursus imperdiet nisl id fermentum. Aliquam pharetra
                  dui laoreet vulputate dignissim. Sed id metus id quam auctor
                  molestie eget ut augue
                </span>
              </div>
            </div>

            <div className="features__content__box">
              <img className="features__content__icon" src={icon2} alt="" />
              <div className="features__content__text-box">
                <span className="features__content__title text_center">
                  Incredibly Powerful Tool
                </span>
                <span className="features__content__text text_center">
                  Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus
                  felis odio, ut volutpat massa placerat ac. Curabitur dapibus
                  iaculis mi gravida luctus. Aliquam erat volutpat.
                </span>
              </div>
            </div>

            <div className="features__content__box">
              <img className="features__content__icon" src={icon3} alt="" />
              <div className="features__content__text-box">
                <span className="features__content__title text_center">
                  Experimental Stuff
                </span>
                <span className="features__content__text text_center">
                  Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus
                  felis odio, ut volutpat massa placerat ac. Curabitur dapibus
                  iaculis mi gravida luctus. Aliquam erat volutpat.
                </span>
              </div>
            </div>

            <div className="features__content__box">
              <img className="features__content__icon" src={icon4} alt="" />
              <div className="features__content__text-box">
                <span className="features__content__title text_center">
                  Creatred to Make The Web Better
                </span>
                <span className="features__content__text text_center">
                  Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus
                  felis odio, ut volutpat massa placerat ac. Curabitur dapibus
                  iaculis mi gravida luctus. Aliquam erat volutpat.
                </span>
              </div>
            </div>
          </div>
          <h2 className="section-title features__title-image text_center">
            &lt;Here is what you get&gt;
          </h2>
          <img className="features__content__image" src={icon5} alt="" />
        </div>
      </div>
    </section>
  );
}

export default Features;
