function Headerlogo() {
  return (
    <div className="header-topmenu__logo">
      <a className="header-topmenu__logo__link" href="#">
        <div className="header-topmenu__logo__icon"></div>
        <span className="header-topmenu__logo__text">forkio</span>
      </a>
    </div>
  );
}

export default Headerlogo;
