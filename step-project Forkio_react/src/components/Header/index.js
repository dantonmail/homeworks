import Headerlogo from "../Headerlogo";
import Headernav from "../Headernav";
import Headercontent from "../Headercontent";
import Headerburger from "../Headerburger";
import Headerbutton from "../Headerbutton";

function Header() {
  return (
    <header className="header">
      <div className="container container__header">
        <div className="header-topmenu">
          <Headerlogo />
          <Headerbutton />
          <Headerburger />
          <Headernav />
        </div>
        <Headercontent />
      </div>
    </header>
  );
}
export default Header;
