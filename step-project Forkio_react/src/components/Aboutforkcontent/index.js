function Aboutforkcontent(props) {
  return (
    <div className="about-fork__content__box">
      <img className="about-fork__icon" src={props.image} alt="just image" />
      <span className="about-fork__text text_center">
        {props.TextAboutfork}
      </span>
    </div>
  );
}

export default Aboutforkcontent;
