function Pricing() {
  return (
    <section className="pricing">
      <div className="container container_pricing">
        <h2 className="section-title pricing__title flex-center">
          Fork Subscription Pricing
        </h2>
        <div className="pricing__content__wrapper">
          <div className="pricing__content__box">
            <span className="pricing__content__title">STUDENTS</span>
            <span className="pricing__content__price">$29</span>
            <p className="pricing__content__price-text">PER MONTH</p>
            <div className="pricing__content__type__oneline">
              Personal License
            </div>
            <button className="pricing__content__btn">PURCHASE</button>
          </div>
          <div className="pricing__content__box">
            <span className="pricing__content__title">professional</span>
            <span className="pricing__content__price">$59</span>
            <p className="pricing__content__price-text">PER MONTH</p>
            <div className="pricing__content__type">
              <p>Professional License</p> <p>Email Support</p>
            </div>
            <button className="pricing__content__btn">PURCHASE</button>
          </div>
          <div className="pricing__content__active_box">
            <span className="pricing__content__title pricing__content__title__active">
              agency
            </span>
            <span className="pricing__content__price pricing__content__price__active">
              $99
            </span>
            <p className="pricing__content__price-text">PER MONTH</p>
            <div className="pricing__content__type">
              <p>1-12 Team Members</p> <p>Phone Support</p>
            </div>
            <button className="pricing__content__btn pricing__content__btn-active">
              PURCHASE
            </button>
          </div>
          <div className="pricing__content__box">
            <span className="pricing__content__title">enterprise</span>
            <span className="pricing__content__price">$159</span>
            <p className="pricing__content__price-text">PER MONTH</p>
            <div className="pricing__content__type">
              <p>Unlimited Team Members</p> <p>24/7 Phone Support</p>
            </div>
            <button className="pricing__content__btn">PURCHASE</button>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Pricing;
