function Headercontent() {
  return (
    <div className="header-content">
      <h1 className="section-title header-content__title text_center">
        Fork App
      </h1>
      <h3 className="header-content__subtitle text_center">
        A Real Gamechanger In The World Of Web-Development
      </h3>
      <span className="header-content__text text_center">
        v. 2.8 For Mac and Windows
      </span>

      <button className="header-content__button header-content__button button_header">
        Download For Free Now
      </button>
      <span className="header-content__text__bottom text_center">
        Unlimited 30-Days Trial Period
      </span>
    </div>
  );
}
export default Headercontent;
