"use strict";

function createActorFile() {
  let actor = [];
  let actorName = [];
  let characterFile = [];
  let target = document.getElementsByClassName("films");

  async function getFilmData(url) {
    // делаем запрос по ссылке, получаем фильмы
    let response = await fetch(url);
    let data = await response.json();

    let film = data.results.map((e) => {
      // получем наименование фильмов, id и описание
      return `<li class ="films" data-id ="${e.episode_id}"><ins>"Episode:"</ins> -${e.episode_id} <br> <ins>"Film name:"</ins> -"${e.title}" <br> <ins>"Description:"</ins> -${e.opening_crawl}<br><ins>"Actors:"</ins> - </li>`;
    });

    characterFile = data.results.map((item) => {
      // забираем массив со ссылками ко всем фильмам
      return item.characters;
    });

    function findKey(y) {
      for (let key in y) {
        actorName = characterFile[key].map((elem) => {
          //перебираем массив со ссылками  к фильмам
          async function getActorName(x) {
            // делаем запросы по ссылками к к фильмам
            let response = await fetch(x);
            let data = await response.json();
            // получаем актеров к фильмам
            actor = data.name;
            return `${actor}, `;
          }

          getActorName(elem).then((actor) =>
            target[key].insertAdjacentHTML("beforeend", actor)
          );
        });
      }
    }
    findKey(characterFile);

    document.body.insertAdjacentHTML("beforeend", `<ul>${film.join("")}</ul>`);
  }

  getFilmData("https://swapi.dev/api/films/");
}
createActorFile();
