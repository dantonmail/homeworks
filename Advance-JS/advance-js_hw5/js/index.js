"use strict";

//Обьясните своими словами, как вы понимаете асинхронность в Javascript

// Поскольку JS язык однопоточный, то програмный код выполняется последовательно. Только одна конкретная операция происходит в данный момент времени. Асинхронное программирование позволяет выполнять несколько операций в конкретный момент времени (передавая некоторые процессы в Web ip и не тормозя выполнение основного потока). Например позволяет откладывать дальнейшие операции, пока предыдущая не выполнится а так же по истечению заданного времени, или с регулярным интервалом (использование setTimeout, промисов и асинхронных функций "async")

function getClientData() {
  let button = document.querySelector(".button");
  let result = document.querySelector(".results");
  let ip;

  button.addEventListener("click", () => {
    async function getIp(url) {
      let ipArdess = await fetch(url);
      let ipData = await ipArdess.json();
      ip = ipData.ip;
      let clien = await fetch(`http://ip-api.com/json/${ip}`);
      let clientData = await clien.json();
      result.insertAdjacentHTML(
        "beforeend",
        `<ins>CLIENT DATA</ins> <br><br> Conurty:"${clientData.country}" <br>Counrty code:"${clientData.countryCode}"<br>Country region:"${clientData.region}"<br>Region:"${clientData.regionName}"<br>City:"${clientData.city}" <br> Provider name:"${clientData.org}"`
      );
    }
    getIp("https://api.ipify.org/?format=json");
  });
}

getClientData();
