"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    return this.name;
  }
  getAge() {
    return this.age;
  }
  getSalary() {
    return `eur${this.salary}`;
  }
  getInfo() {
    return `name: ${this.name} age: ${this.age} salary: eur${this.salary}`;
  }
  setSalary(salary) {
    return (this.salary = salary);
  }
}

let pit = new Employee("Pit", 37, 900);
console.log(pit.getInfo());

let vasyliy = new Employee("Vasyliy", 23, 900);
vasyliy.setSalary(1000);
console.log(vasyliy.getInfo());

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = [lang];
  }
  getSalary() {
    return `eur${this.salary * 3}`;
  }
  getLang() {
    return this.lang;
  }
  getInfo() {
    return `name: ${this.name} age: ${this.age} salary: eur${
      this.salary * 3
    } lang: ${this.lang}`;
  }
  // setSalary(salary) {
  //   return (this.salary = salary);
  // }
}

let ivan = new Programmer("Ivav", 25, 2000, ["russian", "english"]);

let dimon = new Programmer("Dimon", 35, 1000, [
  "russian",
  "ukraine",
  "english",
]);
let stepan = new Programmer("Stepan", 45, 1000, [
  "english",
  "ukraine",
  "english",
  "spane",
]);

console.log(ivan.getInfo());
console.log(dimon.getInfo());
console.log(stepan.getInfo());

//==============================

// Прототипное наследование, это наследование, при котором в процессе клонирования например объета, копия наследует все характеристики своего прототипа. Т.е. мы клонируем/создаем объект и можем пользоваться свойствами и методами, которые присутсвуют в его родителе.
