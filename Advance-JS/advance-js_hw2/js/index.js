"use strict";

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

function chekBooks(x) {
  let newBooks = x.filter((item, i) => {
    if (item.author && item.name && item.price) {
      return item;
    } else {
      try {
        if (!item.author) {
          throw new SyntaxError("object №" + i + " has no author");
        }
        if (!item.price) {
          throw new SyntaxError("object №" + i + " has no price");
        }
        if (!item.name) {
          throw new SyntaxError("object №" + i + " has no name");
        }
      } catch (error) {
        console.error(error.message);
      }
    }
  });
  return newBooks;
}

let div = document.getElementById("root");
function createList(newBooks) {
  let newArr = newBooks
    .map((item) => {
      return `<li> author: ${item.author} name: ${item.name} price: ${item.price} </li>`;
    })
    .join("");
  return `<ul>${newArr}</ul>`;
}
div.insertAdjacentHTML("beforeend", createList(chekBooks(books)));

// ========================

// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

// -Обычно скрипт останавливается в случае ошибки, с выводом ошибки в консоль.(например неожиданного ввода пользователя, неправильного ответа сервера и т.д) И обычный пользователь не получит никаког понятного уведомления о происходящем. При помощи использования  конструкция try..catch, можно «ловить» ошибки и вместо просто остановки скрипта, вывести например информацию понятную пользователю.

// -При помощи использования  конструкция try..catch можно генерировать собственные ошибки и при правильной организации, предотвращать остановку скрипта с выводом конечной информации об ошибках. В данном случае, описание ошибки может быть каким угодно информативным и понятным.

// -Так же в случае появления ошибки при помощи секции finally, можно выполнить действие, которое произойдет в любом случае: (после try, если не было ошибок или после catch, если ошибки были)? что тоже может быть полезно для пользователя
