import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import {
  DATA_REQUEST_STARTED,
  DATA_REQUEST_SUCCESS,
  DATA_REQUEST_FAILURE,
  ADD_FAVORITE_STARTED,
  ADD_FAVORITE_SUCCESS,
  ADD_FAVORITE_FAILURE,
} from "../types";

const initialStore = {
  data: [],
  favorite: [],
};

function rootReducer(store = initialStore, action) {
  switch (action.type) {
    case DATA_REQUEST_STARTED:
      return {
        ...store,
        loading: true,
      };
    case DATA_REQUEST_SUCCESS:
      return {
        ...store,
        loading: false,
        data: action.payload,
      };
    case DATA_REQUEST_FAILURE:
      return {
        ...store,
        loading: false,
        error: action.payload,
      };
    // ========
    case ADD_FAVORITE_STARTED:
      return {
        ...store,
        loading: true,
      };
    case ADD_FAVORITE_SUCCESS:
      return {
        ...store,
        loading: false,
        favorite: action.payload,
      };
    case ADD_FAVORITE_FAILURE:
      return {
        ...store,
        loading: false,
        error: action.payload,
      };

    default:
      return store;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
