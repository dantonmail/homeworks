import {
  DATA_REQUEST_STARTED,
  DATA_REQUEST_SUCCESS,
  DATA_REQUEST_FAILURE,
  ADD_FAVORITE_STARTED,
  ADD_FAVORITE_SUCCESS,
  ADD_FAVORITE_FAILURE,
} from "../types";
import axios from "axios";

export const dataRequestStarted = () => ({
  type: DATA_REQUEST_STARTED,
});

export const dataRequestSuccess = (data) => ({
  type: DATA_REQUEST_SUCCESS,
  payload: data,
});

export const dataRequestFailure = (error) => ({
  type: DATA_REQUEST_FAILURE,
  payload: error,
});

// ============

export const addFavoriteStarted = () => ({
  type: ADD_FAVORITE_STARTED,
});

export const addFavoriteSuccess = (favorite) => ({
  type: ADD_FAVORITE_SUCCESS,
  payload: favorite,
});

export const addFavoriteFailure = (error) => ({
  type: ADD_FAVORITE_FAILURE,
  payload: error,
});

export const addDataFavorite = (favorite) => {
  return (dispatch) => {
    dispatch(addFavoriteStarted());
    dispatch(addFavoriteSuccess(favorite));
  };
};

// ============

export const dataRequest = () => {
  return (dispatch, getState) => {
    dispatch(dataRequestStarted());

    axios
      .get("../../items.json", [])
      .then((res) => {
        dispatch(dataRequestSuccess(res.data));
      })
      .catch((err) => {
        dispatch(dataRequestFailure(err.message));
      });
  };
};
