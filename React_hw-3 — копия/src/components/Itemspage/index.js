import React from "react";
import Itemlist from "../Itemlist";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { dataRequest } from "../../redux/actions";

function Itemspage({
  setUserFavorite = () => {},
  setUserCart = () => {},
  favorite = [],
  cart = [],
}) {
  // const [data, setData] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(dataRequest());
  }, []);

  const data = useSelector((store) => {
    return store.data;
  });
  console.log(data);

  return (
    <Itemlist
      // setUserFavorite={setUserFavorite}
      // setUserCart={setUserCart}
      data={data}
      // favorite={favorite}
      // cart={cart}
    />
  );
}

export default Itemspage;
