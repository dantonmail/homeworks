import Button from "../Button";

function Header({ openModalHandler1, openModalHandler2 }) {
  return (
    <div className="header">
      <Button
        text="Open first modal"
        bgc="red"
        onClick={() => {
          openModalHandler1();
        }}
        btnClName="button header__button"
      />
      <Button
        text="Open second modal"
        bgc="green"
        onClick={() => {
          openModalHandler2();
        }}
        btnClName="button header__button"
      />
    </div>
  );
}

export default Header;
