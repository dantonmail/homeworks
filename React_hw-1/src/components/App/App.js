import React from "react";
import Button from "../Button";
import Modal from "../Modal";
import Header from "../Header";

class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
  };

  firstModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        firstModal: !state.firstModal,
      };
    });
  };

  secondModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        secondModal: !state.secondModal,
      };
    });
  };

  render() {
    return (
      <>
        <div className="App">
          <Header
            openModalHandler1={this.firstModalHandler}
            openModalHandler2={this.secondModalHandler}
          />

          {this.state.firstModal && (
            <Modal
              text="Once you delete this file, it won't be possible to undo this action. Are you sure  you want to delete it?"
              header="Do you want to delete this file?"
              modalBg="red"
              headerBg="crimson"
              actions={
                <div className="modal__buttons">
                  <Button
                    text="Ok"
                    bgc="crimson"
                    onClick={this.secondModalHandler}
                    btnClName="button modal__button"
                  />
                  <Button
                    text="Cancel"
                    bgc="crimson"
                    onClick={this.secondModalHandler}
                    btnClName="button modal__button"
                  />
                </div>
              }
              closeButton={true}
              closeModalHandler={this.firstModalHandler}
            />
          )}
          {this.state.secondModal && (
            <Modal
              text="Once you save this file, you will ba able to find new properties in your project. Are you sure you want to save it?"
              header="Do you want to save this file?"
              modalBg="green"
              headerBg="darkgreen"
              actions={
                <div className="modal__buttons">
                  <Button
                    text="Ok"
                    bgc="darkgreen"
                    onClick={this.secondModalHandler}
                    btnClName="button modal__button"
                  />
                  <Button
                    text="Cancel"
                    bgc="darkgreen"
                    onClick={this.secondModalHandler}
                    btnClName="button modal__button"
                  />
                </div>
              }
              closeButton={true}
              closeModalHandler={this.secondModalHandler}
            />
          )}
        </div>
      </>
    );
  }
}

export default App;
