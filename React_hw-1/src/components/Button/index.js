import PropTypes from "prop-types";

const Button = ({
  bgc = "black",
  text = "Button",
  btnClName = "button",
  onClick = () => {},
}) => {
  return (
    <button
      onClick={() => {
        onClick();
      }}
      style={{
        backgroundColor: bgc,
      }}
      className={btnClName}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  bgc: PropTypes.string,
};
export default Button;
